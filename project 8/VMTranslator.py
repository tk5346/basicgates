

from Parser import Parser
from CodeWriter import CodeWriter
import os
import sys


class VMTranslator:

    def __init__(self):
        path = self.getCommandLineArgument(1)
        code_writer = CodeWriter(path.replace('.vm', '') + '.asm')

        if os.path.isdir(path):
            files = self.setF(path, 'vm')
            while self.hasMoreFiles():
                filename = self.nextFile()
                VMTranslator.parse(self, filename, code_writer)
        elif os.path.isfile(path):
            VMTranslator.parse(self, path, code_writer)

        code_writer.Close()

    #def main(self):


    # staticmethod
    def parse(self, filename, code_writer):
        parser = Parser(filename)

        while parser.hasMoreCommands():
            parser.advance()

            if parser.commandType() in ['C_PUSH', 'C_POP']:
                code_writer.WritePushPop(parser.command(), parser.arg1(), parser.arg2())
            elif parser.commandType() == 'C_ARITHMETIC':
                code_writer.writeArithmetic(parser.command())

    def setF(self, filename, ext):
        self.target_ext = "." + ext
        (self.fname, self.ext) = ("test", "")  # default
        if (filename):
            (self.fname, self.ext) = os.path.splitext(filename)

        # Determine if a file or directory was supplied
        self.type_file = False
        self.type_dir = False
        if (self.ext):
            if (self.ext == self.target_ext):
                self.type_file = True
        else:
            self.type_dir = True

        self.fileList = []
        self.dirList = []

        # Supplied name is a file of the correct extension
        if (self.type_file):
            if (os.path.isfile(filename)):
                self.dirList = [filename]

        # Supplied name is a directory
        if (self.type_dir):
            if (os.path.isdir(self.fname)):
                # os.chdir(self.fname)
                # self.dirList = os.listdir('.')
                self.dirList = os.listdir(self.fname)

        for filename in self.dirList:
            if (os.path.splitext(filename)[1] == self.target_ext):
                if self.type_dir:
                    filename = os.path.join(self.fname, filename)
                self.fileList.append(filename)



    def report(self):
        if (self.type_file):
            print("Processing FILE: %s" % self.fname)
        if (self.type_dir):
            print("Processing DIRECTORY: %s" % self.fname)

        print("Files: %i" % len(self.fileList))

        for filename in self.fileList:
            print("  %s" % os.path.basename(filename))

    def hasMoreFiles(self):
        if self.fileList:
            return True
        return False

    def nextFile(self):
        filename = None
        if self.fileList:
            filename = self.fileList[0]
            self.fileList.remove(filename)
        return filename

    def basename(self):
        return self.fname

    @staticmethod
    def getCommandLineArgument(num):
        arg = None
        if num < len(sys.argv):
            arg = sys.argv[num]
        return arg

    @staticmethod
    def repeatedChar(char, count):
        string = ""
        for each in range(count):
            string += char
        return string

VMTranslator()